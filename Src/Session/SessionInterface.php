<?php

namespace Ds\Session;

/**
 * Interface SessionStorageInterface
 *
 * @package Ds\Session
 */
interface SessionInterface extends SessionStorageInterface
{
    /**
     * Create a new session;
     *
     * @param SessionStorageInterface $sessionStorage
     * @return SessionInterface
     */
    public static function init(SessionStorageInterface $sessionStorage) : SessionInterface;
}
