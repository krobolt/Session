<?php

namespace Ds\Session;

/**
 * Interface SessionStorageInterface
 *
 * @package Ds\Session
 */
interface SessionStorageInterface
{
    /**
     * Start a session if not already started.
     *
     * @param array $options
     *
     * @return void
     */
    public function start(array $options = []);

    /**
     * Check if session key exists
     *
     * @param $key
     *
     * @return bool
     */
    public function has(string $key);

    /**
     * Set session key.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return void
     */
    public function set(string $key, $value);

    /**
     * Return a session key Returns $default if it has not been created.
     *
     * @param $key
     * @param bool $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * Delete session key.
     *
     * @param string $key
     *
     * @return void
     */
    public function delete(string $key);

    /**
     * Returns current Session Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Returns current session name.
     *
     * @return string
     */
    public function getName();

    /**
     * Regenerate session id. Remove old session data by default.
     * @param bool|string $id Session id.
     *
     * @return void
     */
    public function regenerate($id = false);

    /**
     * Destroy Current Session
     *
     * @return void
     */
    public function destroy();
}
