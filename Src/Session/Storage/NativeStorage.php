<?php

namespace Ds\Session\Storage;

use Ds\Session\SessionStorageInterface;

/**
 * Class Native Session Storage
 *
 * @package Ds\Session
 */
class NativeStorage implements SessionStorageInterface
{
    /**
     * Store Session Values. Cannot be accessed directly.
     * @var &$_SESSION $session
     */
    private $session;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * NativeStorage constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @inheritdoc
     */
    public function start(array $options = [])
    {
        $options = empty($options) ? $this->options : $options;

        $this->updateCookieSessionParams($options);

        if ($this->isStarted() === false) {
            \session_start($options);
            $_SESSION['started'] = true;
        }

        $this->session = &$_SESSION;

    }

    /**
     * Update Session cookie params.
     * @param array $options
     */
    public function updateCookieSessionParams(array $options){

        if (!isset($options['cookie'])) {
            return;
        }

        $defaults = \session_get_cookie_params();

        foreach ($defaults as $param => $default){
            if ( isset( $options['cookie'][$param] ) ){
                $defaults[$param] = $options['cookie'][$param];
            }
        }

        \session_set_cookie_params(
            $defaults['lifetime'],
            $defaults['path'],
            $defaults['domain'],
            $defaults['secure'],
            $defaults['httponly']
        );
    }

    /**
     * @inheritdoc
     */
    public function has(string $key)
    {
        if (isset($this->session[$key])) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function set(string $key, $value)
    {
        $this->session[$key] = $value;
    }

    /**
     * @inheritdoc
     */
    public function get(string $key, $default = null)
    {
        if (isset($this->session[$key])) {
            return $this->session[$key];
        }

        return $default;
    }

    /**
     * @inheritdoc
     */
    public function delete(string $key){
        unset($this->session[$key]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return \session_id();
    }

    /**
     * @inheritdoc
     */
    public function getName(){
        return \session_name();
    }

    /**
     * @inheritdoc
     */
    public function destroy()
    {
        $this->session = [];
        if (!$this->isStarted()){
            \session_start();
        }
        \session_destroy();

    }

    /**
     * @inheritdoc
     */
    public function regenerate($id = false)
    {
        if ($this->sessionIsStarted()){
            session_regenerate_id($id);
        }
    }

    /**
     * Check if session has already been started.
     */
    public function isStarted()
    {
        if (\session_status() == PHP_SESSION_ACTIVE || isset($_SESSION['started'])) {
            return true;
        }
        return false;
    }


}
