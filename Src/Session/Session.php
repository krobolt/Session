<?php

namespace Ds\Session;

/**
 * Class Session
 *
 * @package Ds\Session
 */
class Session implements SessionInterface
{
    /**
     * @var SessionStorageInterface $storage
     */
    private $storage;

    /**
     * Session constructor.
     * @param SessionStorageInterface $sessionStorage
     */
    public function __construct(SessionStorageInterface $sessionStorage)
    {
        $this->storage = $sessionStorage;
    }

    /**
     * @inheritdoc
     */
    public static function init(SessionStorageInterface $sessionStorage) : SessionInterface
    {
        return new Session($sessionStorage);
    }

    /**
     * @inheritdoc
     */
    public function start(array $options = [])
    {
        $this->storage->start($options);
    }

    /**
     * @inheritdoc
     */
    public function has(string $key){
        return $this->storage->has($key);
    }

    /**
     * @inheritdoc
     */
    public function set(string $key, $value)
    {
        $this->storage->set($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function get(string $key, $default = null)
    {
        return $this->storage->get($key, $default);
    }

    /**
     * @inheritdoc
     */
    public function delete(string $key){
        $this->storage->delete($key);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->storage->getId();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->storage->getName();
    }

    /**
     * @inheritdoc
     */
    public function regenerate($id = false)
    {
        $this->storage->regenerate($id);
    }

    /**
     * @inheritdoc
     */
    public function destroy()
    {
        $this->storage->destroy();
    }
}
