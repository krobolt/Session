<?php

namespace Tests\Session;

use Ds\Session\Session;
use Ds\Session\SessionStorageInterface;

class SessionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var SessionStorageInterface
     */
    public $storage;

    /**
     * @var Session
     */
    public $session;

    public function setUp()
    {
        $this->storage = $this->getMockBuilder(SessionStorageInterface::class)
            ->getMock();
        $this->session = Session::init($this->storage);
    }

    /**
     * Test that init correctly produces new instance of Rs\Session\SessionInterface
     */
    public function testInit(){
        $session = Session::init($this->storage);
        $this->assertInstanceOf(Session::class, $session,'Init does not produce an instance of Rs\Session\SessionInterface');
    }

    /**
     * Test that SessionStorageInterface is called.
     */
    public function testStart()
    {
        $this->storage->expects($this->once())
            ->method('start');

        $this->session->start();
    }

    /**
     * Test that SessionStorageInterface has item
     */
    public function testHas()
    {
        $key = 'foo';
        $value = true;

        $this->storage->expects($this->once())
            ->method('has')
            ->with(
                $this->equalTo($key)
            )
            ->will($this->returnValue($value));

        $this->session->has($key);
    }

    public function testSet()
    {
        $key = 'foo';
        $value = 'bar';
        $this->storage->expects($this->once())
            ->method('set')
            ->with(
                $this->equalTo($key),
                $this->equalTo($value)
            );
        $this->session->set($key, $value);
    }

    public function testGet()
    {
        $key = 'foo';
        $default = 'bar';
        $this->storage->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($key),
                $this->equalTo($default)
            );
        $this->session->get($key, $default);
    }

    public function testGetId()
    {
        $this->storage->expects($this->once())
            ->method('getId');
        $this->session->getId();
    }

    public function testGetName(){
        $this->storage->expects($this->once())
            ->method('getName');
        $this->session->getName();
    }

    public function testRegenerate()
    {
        $deleteOld = false;

        $this->storage->expects($this->once())
            ->method('regenerate')
            ->with(
                $this->equalTo($deleteOld)
            );

        $this->session->regenerate($deleteOld);
    }

    public function testDestroy()
    {
        $this->storage->expects($this->once())
            ->method('destroy');
        $this->session->destroy();
    }
}
