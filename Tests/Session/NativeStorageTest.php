<?php

namespace Tests\Session;

use Ds\Session\Storage\NativeStorage;

class NativeStorageTest extends \PHPUnit_Framework_TestCase
{

    public $storage;

    public function setUp()
    {
        unset($_COOKIE);
        unset($_SESSION);

        $_SESSION = array();

        @session_start();
        @session_destroy();
    }

    public function testSessionStart()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();
        $this->assertEquals(true, $this->storage->isStarted());
    }

    public function testSessionDestroy()
    {
        $this->storage = new NativeStorage();
        $this->storage->destroy();
        $this->assertEquals(false, $this->storage->isStarted());
    }

    public function testHas()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();
        $_SESSION['foo'] = 'someValue';
        $expected = true;
        $actual = $this->storage->has('foo');
        $this->assertEquals($expected, $actual);
    }

    public function testHasNoValue()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();
        $expected = null;
        $actual = $this->storage->has('someRandom');
        $this->assertEquals($expected, $actual);
    }

    public function testSet()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();
        $expected = 'data';
        $this->storage->set('foo', 'data');
        $actual = $_SESSION['foo'];
        $this->assertEquals($expected, $actual);
    }

    public function testGet()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();

        $expected = 'bar';
        $_SESSION['foo'] = $expected;

        $actual = $this->storage->get('foo');
        $this->assertEquals($expected, $actual);
    }

    public function testGetId()
    {
        $this->storage = new NativeStorage();
        $expected = session_id();
        $actual = $this->storage->getId();
        $this->assertEquals($expected, $actual);
    }

    public function testDestroy()
    {
        $this->storage = new NativeStorage();
        $this->storage->start();
        $id = $this->storage->getId();
        $this->storage->destroy();
        $this->storage->start();
        $new = $this->storage->getId();
        $this->assertNotEquals(
            $id, $new
        );
    }
}
