[![SensioLabsInsight](https://insight.sensiolabs.com/projects/f344a3c5-1499-4901-a4ec-399af0ae6d16/mini.png)](https://insight.sensiolabs.com/projects/f344a3c5-1499-4901-a4ec-399af0ae6d16)
[![Code Climate](https://codeclimate.com/github/djsmithme/Session/badges/gpa.svg)](https://codeclimate.com/github/djsmithme/Session)
[![Test Coverage](https://codeclimate.com/github/djsmithme/Session/badges/coverage.svg)](https://codeclimate.com/github/djsmithme/Session/coverage)
[![Issue Count](https://codeclimate.com/github/djsmithme/Session/badges/issue_count.svg)](https://codeclimate.com/github/djsmithme/Session)

# Session
Session Component.
